#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int s, t, n, m, a ,b;
char visitados[30013];
int prev[30013];
std::vector<int> g[n+2];

void dfs(int u){
	visitados[u] = 1;
	for (int j = 0; j <= n+1; ++j){
		if(g[u][j] && !visitados[j]){
			dfs(j);
			prev[j] = u;
		}
	}
}

int ford_fulkerson(){
	int cont = 0;		
	while(true){		
		for (int i = 0; i <= n+1; ++i){ 
			prev[i] = -1;
			visitados[i] = 0;
		}
		
		dfs(s);
		
		if(prev[t] == -1){			
			break;			
		} 
		
		for (int v = t; prev[v] != -1; v = prev[v]){			
			g[prev[v]][v] = 0;
			g[v][prev[v]] = 1;			
		}
		cont++;
	}
	return cont;

}

int main(){
	int testes, res;
	scanf("%d", &testes);
	s = 2;
	
	
	for (int j = 0; j < testes; ++j){
		scanf("%d %d", &n, &m);		
		t = n+1;		
		g[1][t] = 1;
		g[3][t] = 1;

		for(int i = 1; i <= m; i++){
			scanf("%d %d", &a, &b);
			g[a][b] = 1;
			g[b][a] = 1;
		}

		res = ford_fulkerson();
		if(res > 1) printf("YES\n");
		else printf("NO\n");
	
	}
	

	return 0;
}