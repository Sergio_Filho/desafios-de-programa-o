#include <stdio.h>
#include <iostream>
using namespace std;
int T, N;
int* v;
long long int sum;
long long int total;
int main(){
	scanf("%d", &T);	
	for (int i = 1; i <= T; ++i){		
		scanf("%d", &N);
		sum = 0;
		total = 0;
		v = new int[N+1];				
		
		for (int i = 1; i <=N; ++i){
			scanf("%d", &v[i]);
			v[i] = v[i]%1300031;
			sum += v[i];
		}

		for (int i = 1; i <=N; ++i){
			total += sum*v[i];
			total = total%1300031;
			sum -= v[i];
		}
		

		printf("\n%lld", total);

		delete [] v;
		
	}

}