#include <stdio.h>
#include <iostream>
using namespace  std;

int val[101];
int v1,v2, N;

bool calcSum(int* valores, int k){
	bool vSoma[k+1];
	int vParticipantes[k+1];
	
	for (int i = 1; i <= k; ++i)
		vSoma[i] = false;	
	vSoma[0] = true;

	for (int j = 1; j <= N ; ++j){
		for (int i = k; i>=0; --i){
			if (vSoma[i]){	
				int v = i+valores[j];				
				if(v<=k){
					vSoma[v] = true;
					vParticipantes[v] = j;
				}
				if (v == k) return true;
			}
		}	
	}
	
	return false;
	
}

int main(){
	int soma;
	int k;
	int j=1;
	while(true){		
		scanf("%d %d %d", &v1, &v2, &N);
		soma = v1+v2;
		
		if(N==0 && v1==0 && v2==0)
			break;

		for (int i = 1; i <= N; ++i){
			scanf("%d", &val[i]);
			soma += val[i];
		}
		if(soma % 2 == 1){
			printf("Teste %d\nN\n\n",j);
		}
		else{
			soma = soma/2;
			k = v1>v2 ? soma-v1 : soma-v2;			
			char c = calcSum(val, k) ? 'S' : 'N'; 
			printf("Teste %d\n%c\n\n",j,c);				 
		}
		j++;

	}

}