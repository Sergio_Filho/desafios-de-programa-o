#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <map>

using namespace std;

int n, m;
int * F;
int ** AF;


int brute_force(char * text, char * pattern)
{
  int i,j;
  int n = strlen(text);
  int m = strlen(pattern);

  for(i = 0; i < n; i++)
  {
    printf("%s\n", text);

    for(int k = 0; k < i; k++)
      printf(" ");

    //printf("%s\n", pattern);
    for(j = 0; j < m && i+j < n; j++)
    {
      printf("%c", pattern[j] );
      if( text[i+j] != pattern[j] )
      {
        printf("\nfalha\n");
        break;
      }

    }

    if( j == m )
    {
      printf("\nmatch found %d\n", i);
      return i;
    }
  }


  return -1;
}



void build_failure_function(char * pattern)
{
  int i,j;
  int m;

  m = strlen(pattern);
  F = (int *)malloc( m*sizeof(int) );

  F[ 0 ] = 0;

  j = 0; // indices prefixos
  i = 1; // indices sufixos

  while( i < m )
  {
    if( pattern[i] == pattern[j] )
    {
      F[i++] = ++j;
    }
    else if( j == 0)
    {
      F[i++] = 0;
    } else {
      j = F[j-1];
    }
  }

  printf("failure function\n");
  for(int i = 0; i < m; i++)
    printf("%d", F[i] );
  printf("\n");

}

int kmp(char * text, char * pattern)
{
  int i, j;
  int n, m;

  n = strlen(text);
  m = strlen(pattern);

  build_failure_function(pattern);

  i = 0;
  j = 0;

  while(i < n)
  {

    printf("i %d j %d\n", i, j);

    if( text[i] == pattern[j] )
    {
      if( j == m-1)
      {
        printf("match found %d\n", i - j);
        return i;
      }
      else
      {
        i++;
        j++;
      }
    }else{
      if( j != 0)
      {
        j = F[j-1];
      }
      else{
        i++;
      }
    }
  }

  return -1;
}

int get_next_state(char * pat, char * sigma, int m, int state, int x)
{
  if( state < m && sigma[x] == pat[state] )
  {
    return state + 1;
  }

  int ns, i;

  //ns contem o maior prefixo que � suffixo em pat[state-1]c
  for(ns = state; ns > 0; ns--)
  {
    if( pat[ns-1] == sigma[x] )
    {
      for(i = 0; i < ns-1; i++)
      {
        if( pat[i] != pat[state-ns+i+1])
        {
          break;
        }
      }

      if( i == ns-1)
      {
        return ns;
      }

    }

  }

  return 0;

}

void compute_dfa(char * pattern, char * sigma)
{
  int m;
  int no_of_chars;

  m = strlen(pattern);
  no_of_chars = strlen(sigma);

  AF = (int **)malloc( (m+1)*sizeof(int*) );
  for(int state = 0; state <= m; state++)
    AF[state] = (int *)malloc( (no_of_chars)*sizeof(int) );

  printf("%s", "state");
  for(int x = 0; x < no_of_chars; x++)
  {
    printf("%3c ", sigma[x]);
  }
  printf("\n");

  for(int state = 0; state <= m; state++)
  {
    printf("  %d  ", state );
    for(int x = 0; x < no_of_chars; x++)
    {
      AF[state][x] = get_next_state(pattern, sigma, m, state, x);
      printf(" %3d", AF[state][x] );
    }
    printf("\n");
  }


}

void search_dfa(char * pat, char * sigma, char * txt)
{
  int n, m, no_of_chars;

  map <char, int> mapa;

  n = strlen(txt);
  m = strlen(pat);


  compute_dfa(pat, sigma);

  printf("txt %s\n", txt );
  printf("pat %s\n", pat );

  no_of_chars = strlen(sigma);

  for(int i = 0; i < no_of_chars; i++)
    mapa[ sigma[i] ] = i;

  int state = 0;
  for(int i = 0; i < n; i++)
  {
    state = AF[state][ mapa[ txt[i] ] ];
    printf("state %d\n", state  );
    if( state == m)
    {
      printf("pattern found %d\n", i-m+1);
    }
  }

}



int main()
{
  char * T = "abababacaba";
  char * P = "ababaca";
  char * S = "abc";

  //brute_force(T, P);

  //printf("\n");

  //kmp(T, P);

  //printf("\n");

  //compute_dfa(P, S);

  //search_dfa(P, S, T);

}
