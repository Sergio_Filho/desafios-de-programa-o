#include <stdio.h>

int expmod(int a, int b, int n)
{
  if(b==1)
  {
    return 1;
  }
  else
  {
    long long int res = expmod(a, b/2, n);
    res = (res*res) % n;
    if(b %2 == 1)
      res = (res*a) % n;
    return (int)res;
  }
}

int euclides(int a, int b)
{
  if(a<0) a = -a;
  if(b<0) b = -b;

  while(b > 0)
  {
    int r = a%b;
    a = b;
    b = r;
  }

  return a;
}

int euclides_estendido(int a, int b, int & X, int &Y, int &Z)
{

  int x,y,z;
  int q,t;

  X = 1; Y = 0; Z = a;
  x = 0; y = 1; z = b;

  while( z > 0)
  {
    q = Z/z;

    t = Z - q*z;
    Z = z;
    z = t;

    t = X - q*x;
    X = x;
    x = t;

    t = Y - q*y;
    Y = y;
    y = t;

  }


}




int main()
{
  int a, b, s, t, d;

  scanf("%d %d", &a, &b);

  euclides_estendido(a,b,s,t,d);

  printf("(%d, %d, %d)\n", s, t, d);

}
