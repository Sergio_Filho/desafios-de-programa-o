#include <stdio.h>
#include <vector>
#include <queue>
#define MAXN 100
#define INF 100001
using namespace std;

int n, m;

typedef struct Edge {
  int from, to,cap;
  Edge(int from, int to, int cap): from(from), to(to), cap(cap) {};
};

vector <Edge> adj[MAXN];
bool marc[MAXN];
int dist[MAXN];
int pai[MAXN];

int djikstra(int s, int t)
{
  priority_queue < pair<int, int> > heap;
  fill(marc, marc + n, false);
  fill(dist, dist + n, INF);
  fill(pai, pai + n , -1);

  dist[s] = 0;
  heap.push( make_pair(0, s) );

  while( !heap.empty() )
  {
    int v = heap.top().second;
    heap.pop();
    if( marc[v] ) continue;

    if(v==t) break;

    marc[v] = true;

    for(int k = 0; k < adj[v].size(); k++)
    {
      Edge e = adj[v][k];
      if( dist[e.to] > dist[e.from] + e.cap )
      {
        dist[e.to] = dist[e.from] + e.cap;
        pai[e.to] = e.from;
        heap.push( make_pair(-dist[e.to], e.to) );
      }
    }
  }

  return dist[t];
}

int main()
{
  int s, t;
  int sol;

  while(1)
  {
    scanf("%d %d",&n,&m);

    if( n== 0 && m==0) break;

    for(int i = 0; i < m; i++)
    {
      int u,v,cap;
      scanf("%d %d %d", &u, &v, &cap);
      u--;
      v--;
      adj[u].push_back( Edge(u, v, cap));
    }

    scanf("%d %d", &s, &t);
    s--;
    t--;
    sol = djikstra(s,t);

    if( sol != INF)
      printf("%d\n", sol);
    else
      printf("-1\n");

    for(int i = 0; i < n; i++) adj[i].clear();

  }

  return 0;

}
