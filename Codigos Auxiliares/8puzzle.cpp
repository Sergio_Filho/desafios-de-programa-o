#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <stack>
#include <set>
#include <queue>
#include <map>
using namespace std;

enum Direcao {Direita, Esquerda, Cima, Baixo};

class Estado {
  private:
    int table[3][3];
    int vazio;
  public:
    Estado( vector <int> v);
    Estado( Estado * s);
    void show();
    int posicao(int i, int j) const ;
    void troca(int a, int b, int c, int d);
    Estado * move(Direcao dir);
    pair<int,int> posicao_number(int n)
    {
      for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
          if( table[i][j] == n)
            return make_pair(i,j);
    }


    bool operator< (const Estado& e) const {
      for(int i = 0; i < 3; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if( this->table[i][j] < e.posicao(i,j) ) return true;
          else if( this->table[i][j] > e.posicao(i,j) ) return false;
        }
      }

      return false;
    }

    bool operator==(Estado & e)
    {
      for(int i = 0; i < 3; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if( table[i][j] != e.posicao(i,j) ) return false;
        }
      }
      return true;
    }

    int heuristica(Estado * e)
    {
      int posicoes_erradas = 0;

      for(int i = 0; i < 3; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if( table[i][j] != e->posicao(i,j) ) posicoes_erradas++;
        }
      }
      return posicoes_erradas;
    }

    int heuristica2(Estado * e)
    {
      int soma_distancias = 0;

      for(int i = 1; i <= 8; i++)
      {
        pair<int,int> p1,p2;
        p1 = this->posicao_number(i);
        p2 = e->posicao_number(i);
        soma_distancias += abs(p1.first - p2.first) + abs(p1.second-p2.second);
      }

      return soma_distancias;

    }
};


void Estado::show()
{
  for(int i = 0; i < 3; i++)
  {
    for(int j = 0; j < 3; j++)
      printf("%2d ", table[i][j]);
    printf("\n");
  }
  printf("vazio %d\n", vazio );

}

Estado::Estado( const vector <int> v)
{
  if( v.size() >= 9 )
  {
    for(int i = 0; i < 9; i++)
    {
      if( v[i] >= 1 && v[i] <= 9)
        table[i/3][i%3] = v[i];
      if(v[i]==9) vazio = i;
    }
  }
  else
  {
    cout << "Erro ao inicializar" << endl;
  }
}

Estado:: Estado( Estado * s)
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      table[i][j] = s->posicao(i,j);
}

int Estado::posicao(int i, int j) const
{
  return table[i][j];
}



void Estado::troca(int a, int b, int c, int d)
{
  swap( table[a][b], table[c][d] );
  vazio = 3*c + d;
}

Estado * Estado::move(Direcao dir){
  int a,b;
  bool status;

  a = vazio/3;
  b = vazio%3;

  switch(dir)
  {
    case Direita:  if( b+1 >= 3) return NULL; break;
    case Esquerda: if( b-1 < 0 ) return NULL; break;
    case Cima:     if( a-1 < 0 ) return NULL; break;
    case Baixo:    if( a+1 >= 3) return NULL; break;
  }


  Estado * s = new Estado(this);

  switch(dir)
  {
    case Direita:  s->troca(a,b,a,b+1); break;
    case Esquerda: s->troca(a,b,a,b-1); break;
    case Cima:     s->troca(a,b,a-1,b); break;
    case Baixo:    s->troca(a,b,a+1,b); break;
  }

  return s;

}

class No
{
  public:
    private:
    Estado* s;
    No * pai;
    int nivel;
    Direcao dir;
    public:
    No(Estado* e, No * p, int c, Direcao d);
    No * filho(Direcao d);
    Estado* get_estado() { return this->s; };
    No* get_pai()    { return this->pai; };
    int get_nivel()  { return this->nivel; };
    Direcao get_dir()    { return this->dir; };
    void show();
};

No::No(Estado * e, No * p, int nivel, Direcao d)
{
  this->s = e;
  this->pai = p;
  this->nivel = nivel;
  this->dir = d;
};

No * No::filho(Direcao d)
{
  Estado * prox = s->move(d);
  if( prox == NULL ) return NULL;
  else return new No(prox, this, this->nivel + 1, d);
}


void No::show()
{
  printf("********\n");
  s->show();
  printf("custo %d\n", this->nivel );

  switch(dir)
  {
    case Direita: printf("Direita\n"); break;
    case Esquerda: printf("Esquerda\n"); break;
    case Cima: printf("Cima\n"); break;
    case Baixo: printf("Baixo\n"); break;
  }

  printf("********\n");
}


//Complete: No
//Optimal : No
bool busca_profundidade(Estado * origem, Estado * destino)
{

  stack <No*> s;

  No* inicial = new No(origem, NULL, 0, Direita);

  s.push(inicial);

  while( !s.empty() )
  {
    No* node = s.top(); s.pop();

    if( *node->get_estado() == *destino )
    {
      node->show();
      return true;
    }


    for(int dir = Direita; dir <= Baixo; dir++)
    {
      No * prox = node->filho((Direcao)dir);

      if( prox != NULL )
      {
        s.push(prox);
      }

    }

  }
  return false;
}


//Complete: No
//Optimal : No
//Se um caminho �timo pode ser deletado se n�o for ramificado primeiro.
//Podar apenas os caminhos piores do que j� encontrados
bool busca_profundidade_modificada(Estado * origem, Estado * destino)
{

  stack <No*> s;
  map <Estado, int> custo;
  map <Estado, int>::iterator it;
  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;

  No* inicial = new No(origem, NULL, 0, Direita);

  s.push(inicial);

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.top(); s.pop();

    if( *node->get_estado() == *destino )
    {
      printf("num nodes visitados   : %d\n", num_nodes_visitados);
      printf("num nodes encontrados : %d\n", num_nodes_encontrados);
      node->show();
      return true;
    }


    it = custo.find(*node->get_estado());
    if(  it == custo.end() )
    {
      custo[*node->get_estado()] = node->get_nivel();
      //printf("adiciona estado no map\n");
    }
    else
    {
      //printf("nivel encontrado %d\n", it->second );
      if( node->get_nivel() >= it->second )
      {
        //printf("nivel atual pior do que o nivel j� encontrado\n");
        //printf("poda\n");
        continue;
      }
      else
      {
        //printf("nivel atual melhor que o nivel encontrado");
        custo[*node->get_estado()] = node->get_nivel();
      }

    }

    num_nodes_visitados++;

    //system("PAUSE");


    for(int dir = Direita; dir <= Baixo; dir++)
    {
      No * prox = node->filho((Direcao)dir);

      if( prox != NULL )
      {
        num_nodes_encontrados++;
        s.push(prox);
      }

    }

  }
  return false;
}


//Complete: Yes
//Optimal:  Yes
bool busca_largura(Estado * origem, Estado * destino)
{

  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;

  queue <No*> s;

  No* inicial = new No(origem, NULL, 0, Direita);

  s.push(inicial);

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.front(); s.pop();

    num_nodes_visitados++;

    if( *node->get_estado() == *destino )
    {
      node->show();
      printf("num nodes visitados   : %d\n", num_nodes_visitados);
      printf("num nodes encontrados : %d\n", num_nodes_encontrados);

      return true;
    }

    for(int dir = Direita; dir <= Baixo; dir++)
    {
      No * prox = node->filho((Direcao)dir);

      if( prox != NULL )
      {
        num_nodes_encontrados++;
        s.push(prox);
      }
    }

  }

  printf("num nodes visitados   : %d\n", num_nodes_visitados);
  printf("num nodes encontrados : %d\n", num_nodes_encontrados);
  return false;

}

//Complete: Yes
//Optimal:  Yes
bool busca_largura_modificada(Estado * origem, Estado * destino)
{

  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;
  map <Estado, int> custo;
  map <Estado, int>::iterator it;
  queue <No*> s;

  No* inicial = new No(origem, NULL, 0, Direita);

  s.push(inicial);

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.front(); s.pop();

    num_nodes_visitados++;

    if( *node->get_estado() == *destino )
    {
      node->show();
      printf("num nodes visitados   : %d\n", num_nodes_visitados);
      printf("num nodes encontrados : %d\n", num_nodes_encontrados);

      return true;
    }

    it = custo.find(*node->get_estado());

    if(  it == custo.end() )
    {
      custo[*node->get_estado()] = node->get_nivel();
      //printf("adiciona estado no map\n");
    }
    else
    {
      //printf("nivel encontrado %d\n", it->second );
      if( node->get_nivel() >= it->second )
      {
        //printf("nivel atual pior do que o nivel j� encontrado\n");
        //printf("poda\n");
        continue;
      }
      else
      {
        //printf("nivel atual melhor que o nivel encontrado");
        custo[*node->get_estado()] = node->get_nivel();
      }
    }


    for(int dir = Direita; dir <= Baixo; dir++)
    {
      No * prox = node->filho((Direcao)dir);

      if( prox != NULL )
      {
        num_nodes_encontrados++;
        s.push(prox);
      }
    }

  }

  printf("num nodes visitados   : %d\n", num_nodes_visitados);
  printf("num nodes encontrados : %d\n", num_nodes_encontrados);
  return false;

}


bool busca_profundidade_limitada(Estado * origem, Estado * destino, int nivel_maximo, long long int & num_nodes_visitados, long long int & num_nodes_encontrados)
{

  printf("busca_profundidade_limitada nivel_maximo %d\n", nivel_maximo );
  stack <No*> s;

  No* inicial = new No(origem, NULL,0, Direita);

  s.push(inicial);

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.top(); s.pop();

    num_nodes_visitados++;

    if( *node->get_estado() == *destino )
    {
      node->show();
      return true;
    }

    if( node->get_nivel() < nivel_maximo )
    {
      for(int dir = Direita; dir <= Baixo; dir++)
      {
        No * prox = node->filho((Direcao)dir);

        if( prox != NULL )
        {
            num_nodes_encontrados++;
            s.push(prox);
        }
      }
    }

  }

  return false;

}

bool busca_profundidade_limitada_modificada(Estado * origem, Estado * destino, int nivel_maximo, long long int & num_nodes_visitados, long long int & num_nodes_encontrados)
{


  stack <No*> s;
  map <Estado, int> custo;
  map <Estado, int>::iterator it;

  printf("busca_profundidade_limitada nivel_maximo %d\n", nivel_maximo );


  No* inicial = new No(origem, NULL,0, Direita);

  s.push(inicial);

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.top(); s.pop();

    num_nodes_visitados++;

    if( *node->get_estado() == *destino )
    {
      node->show();
      return true;
    }

    it = custo.find(*node->get_estado());
    if(  it == custo.end() )
    {
      custo[*node->get_estado()] = node->get_nivel();
      //printf("adiciona estado no map\n");
    }
    else
    {
      //printf("nivel encontrado %d\n", it->second );
      if( node->get_nivel() >= it->second )
      {
        //printf("nivel atual pior do que o nivel j� encontrado\n");
        //printf("poda\n");
        continue;
      }
      else
      {
        //printf("nivel atual melhor que o nivel encontrado");
        custo[*node->get_estado()] = node->get_nivel();
      }
    }




    if( node->get_nivel() < nivel_maximo )
    {
      for(int dir = Direita; dir <= Baixo; dir++)
      {
        No * prox = node->filho((Direcao)dir);

        if( prox != NULL )
        {
            num_nodes_encontrados++;
            s.push(prox);
        }
      }
    }

  }

  return false;

}



//Complete: Yes
//Optimal : Yes
bool busca_profundidade_iterativa(Estado * origem, Estado * destino)
{
  int nivel_maximo = 1;
  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;

  while( !busca_profundidade_limitada(origem, destino, nivel_maximo, num_nodes_visitados, num_nodes_encontrados) )
    nivel_maximo++;

  printf("num nodes visitados   : %d\n", num_nodes_visitados);
  printf("num nodes encontrados : %d\n", num_nodes_encontrados);

}


//Complete: Yes
//Optimal : Yes
bool busca_profundidade_iterativa_modificada(Estado * origem, Estado * destino)
{
  int nivel_maximo = 1;
  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;

  while( !busca_profundidade_limitada_modificada(origem, destino, nivel_maximo, num_nodes_visitados, num_nodes_encontrados) )
    nivel_maximo++;

  printf("num nodes visitados   : %d\n", num_nodes_visitados);
  printf("num nodes encontrados : %d\n", num_nodes_encontrados);

}



//Complete : No
//Optimal  : No
bool busca_heuristica(Estado * origem, Estado * destino)
{

  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;

  priority_queue < pair<int,No*> > s;

  No* inicial = new No(origem, NULL, 0, Direita);

  s.push( make_pair( -origem->heuristica(destino), inicial) );

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.top().second;

    s.pop();

    num_nodes_visitados++;

    if( *node->get_estado() == *destino )
    {
      node->show();
      printf("num nodes visitados   : %d\n", num_nodes_visitados);
      printf("num nodes encontrados : %d\n", num_nodes_encontrados);
      return true;
    }

    for(int dir = Direita; dir <= Baixo; dir++)
    {
      No * prox = node->filho((Direcao)dir);

      if( prox != NULL )
      {
        Estado * atual = prox->get_estado();
        s.push( make_pair( -atual->heuristica(destino), prox ) );
        num_nodes_encontrados++;
      }
    }

  }



  return false;
}

//Complete : Yes
//Optimal  : Yes
bool busca_a_estrela(Estado * origem, Estado * destino)
{

  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;

  priority_queue < pair<int,No*> > s;

  No* inicial = new No(origem, NULL, 0, Direita);

  s.push( make_pair( -(origem->heuristica(destino)), inicial) );

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.top().second;

    s.pop();

    num_nodes_visitados++;

    if( *node->get_estado() == *destino )
    {
      node->show();
      printf("num nodes visitados   : %d\n", num_nodes_visitados);
      printf("num nodes encontrados : %d\n", num_nodes_encontrados);
      return true;
    }

    for(int dir = Direita; dir <= Baixo; dir++)
    {
      No * prox = node->filho((Direcao)dir);

      if( prox != NULL )
      {
        Estado * atual = prox->get_estado();
        s.push( make_pair( -( atual->heuristica(destino) + prox->get_nivel() ), prox ) );
        num_nodes_encontrados++;
      }
    }

  }


  return false;
}


//Complete : Yes
//Optimal  : Yes
bool busca_a_estrela_modificada(Estado * origem, Estado * destino)
{

  map <Estado, int> custo;
  map <Estado, int>::iterator it;
  priority_queue < pair<int,No*> > s;
  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;

  No* inicial = new No(origem, NULL, 0, Direita);

  s.push( make_pair( -(origem->heuristica(destino)), inicial) );

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.top().second;

    s.pop();

    num_nodes_visitados++;

    if( *node->get_estado() == *destino )
    {
      node->show();
      printf("num nodes visitados   : %d\n", num_nodes_visitados);
      printf("num nodes encontrados : %d\n", num_nodes_encontrados);
      return true;
    }

    it = custo.find(*node->get_estado());
    if(  it == custo.end() )
    {
      custo[*node->get_estado()] = node->get_nivel();
    }
    else
    {
      if( node->get_nivel() >= it->second )
      {
        continue;
      }
      else
      {
        custo[*node->get_estado()] = node->get_nivel();
      }
    }


    for(int dir = Direita; dir <= Baixo; dir++)
    {
      No * prox = node->filho((Direcao)dir);

      if( prox != NULL )
      {
        Estado * atual = prox->get_estado();
        s.push( make_pair( -( atual->heuristica(destino) + prox->get_nivel() ), prox ) );
        num_nodes_encontrados++;
      }
    }

  }

  printf("num nodes visitados   : %d\n", num_nodes_visitados);
  printf("num nodes encontrados : %d\n", num_nodes_encontrados);
  return false;
}

//Complete : Yes
//Optimal  : Yes
bool busca_a_estrela_modificada2(Estado * origem, Estado * destino)
{

  map <Estado, int> custo;
  map <Estado, int>::iterator it;
  priority_queue < pair<int,No*> > s;
  long long int num_nodes_visitados = 0LL;
  long long int num_nodes_encontrados = 0LL;

  No* inicial = new No(origem, NULL, 0, Direita);

  s.push( make_pair( -(origem->heuristica2(destino)), inicial) );

  num_nodes_encontrados++;

  while( !s.empty() )
  {
    No* node = s.top().second;

    s.pop();

    num_nodes_visitados++;

    if( *node->get_estado() == *destino )
    {
      node->show();
      printf("num nodes visitados   : %d\n", num_nodes_visitados);
      printf("num nodes encontrados : %d\n", num_nodes_encontrados);
      return true;
    }

    it = custo.find(*node->get_estado());
    if(  it == custo.end() )
    {
      custo[*node->get_estado()] = node->get_nivel();
    }
    else
    {
      if( node->get_nivel() >= it->second )
      {
        continue;
      }
      else
      {
        custo[*node->get_estado()] = node->get_nivel();
      }
    }


    for(int dir = Direita; dir <= Baixo; dir++)
    {
      No * prox = node->filho((Direcao)dir);

      if( prox != NULL )
      {
        Estado * atual = prox->get_estado();
        s.push( make_pair( -( atual->heuristica2(destino) + prox->get_nivel() ), prox ) );
        num_nodes_encontrados++;
      }
    }

  }

  printf("num nodes visitados   : %d\n", num_nodes_visitados);
  printf("num nodes encontrados : %d\n", num_nodes_encontrados);
  return false;
}



int main()
{
  clock_t clk;
  double elapsed ;

  /*
  1 3 4
  8 6 2
  7 9 5
  */

  vector <int> v1 = {1,3,4,8,6,2,7,9,5};
  //vector <int> v2 = {1,2,3,8,9,4,7,6,5}; //Facil - Nivel 5
  //vector <int> v2 = {2,8,1,9,4,3,7,6,5}; //Medio -- Nivel 12
  //vector <int> v2 = {2,8,1,4,6,3,9,7,5}; //Dificil -- Nivel 15
  //vector <int> v2 = {5,6,7,4,9,8,3,2,1}; //Extremamente Dificil -- Nivel 27
  /*
  5 9 7
  2 6 8
  3 4 1
  */
  vector <int> v2 = {5,9,7,2,6,8,3,4,1}; //Impossible


  Estado * origem = new Estado(v1);
  Estado * destino = new Estado(v2);

  //clk = clock();
  //busca_profundidade(origem, destino);
  //elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  //printf("time                 = %10.5f\n", elapsed);

  //clk = clock();
  //busca_largura(origem, destino);
  //elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  //printf("time                 = %10.5f\n", elapsed);

  //clk = clock();
  //busca_profundidade_iterativa(origem, destino);
  //elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  //printf("time                 = %10.5f\n", elapsed);

  //clk = clock();
  //busca_a_estrela(origem, destino);
  //elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  //printf("time                 = %10.5f\n", elapsed);

  //clk = clock();
  //busca_profundidade_modificada(origem, destino);
  //elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  //printf("time                 = %10.5f\n", elapsed);

  clk = clock();
  busca_largura_modificada(origem, destino);
  elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  printf("time                 = %10.5f\n", elapsed);

  //clk = clock();
  //busca_profundidade_iterativa_modificada(origem, destino);
  //elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  //printf("time                 = %10.5f\n", elapsed);

  clk = clock();
  busca_a_estrela_modificada(origem, destino);
  elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  printf("time                 = %10.5f\n", elapsed);

  clk = clock();
  busca_a_estrela_modificada2(origem, destino);
  elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  printf("time                 = %10.5f\n", elapsed);

}
