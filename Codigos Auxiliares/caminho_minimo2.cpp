#include <stdio.h>
#include <vector>
#include <queue>
#define MAXN 1002
#define INF 1002000
using namespace std;

int n, m;

int g[MAXN][MAXN];
bool marc[MAXN];
int dist[MAXN];
int pai[MAXN];

int djikstra(int s, int t)
{
  priority_queue < pair<int, int> > heap;
  fill(marc, marc + n, false);
  fill(dist, dist + n, INF);
  fill(pai, pai + n , -1);

  dist[s] = 0;
  heap.push( make_pair(0, s) );

  while( !heap.empty() )
  {
    int v = heap.top().second;

    heap.pop();

    if( marc[v] ) continue;

    if(v==t) break;

    marc[v] = true;


    for(int u = 0; u < n; u++)
    {
      if( dist[u] > dist[v] + g[v][u] )
      {
        dist[u] = dist[v] + g[v][u];
        pai[u] = v;
        heap.push( make_pair(-dist[u], u) );
      }
    }
  }

  return dist[t];
}

int main()
{
  int p;
  int sol;

  scanf("%d %d",&p,&m);

  n = p+2;

  for(int i = 0; i < n; i++)
    for(int j = 0; j < n; j++)
      g[i][j] = INF;

  for(int i = 0; i < m; i++)
  {
    int u,v,cost;
    scanf("%d %d %d", &u, &v, &cost);
    g[u][v] = cost;
    g[v][u] = cost;
  }

  sol = djikstra(0,n-1);

  printf("%d\n", sol);

  return 0;

}
