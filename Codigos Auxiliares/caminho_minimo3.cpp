#include <stdio.h>
#include <vector>
#include <queue>
#define MAXN 1002
#define INF 1002000
using namespace std;

int n, m;

int g[MAXN][MAXN];
bool marc[MAXN];
int dist[MAXN];
int pai[MAXN];
int cost[MAXN][MAXN];
int adj[MAXN][MAXN];
int deg[MAXN];


int djikstra(int s, int t)
{
  priority_queue < pair<int, int> > heap;
  fill(marc, marc + n, false);
  fill(dist, dist + n, INF);
  fill(pai, pai + n , -1);

  dist[s] = 0;
  heap.push( make_pair(0, s) );

  while( !heap.empty() )
  {
    int v = heap.top().second;

    heap.pop();

    if( marc[v] ) continue;

    if(v==t) break;

    marc[v] = true;

    //printf("vertice %d dist %d\n", v, dist[v]);

    for(int k = 0; k < deg[v]; k++)
    {
      int u = adj[v][k];

      if( dist[u] > dist[v] + cost[v][u] )
      {
        dist[u] = dist[v] + cost[v][u];
        pai[u] = v;
        //printf("atualiza vertice %d dist %d\n", u, dist[u]);
        heap.push( make_pair(-dist[u], u) );
      }
    }
  }

  return dist[t];
}

int main()
{
  int p;
  int sol;

  scanf("%d %d",&n,&m);


  for(int i = 0; i < n; i++)
  {
    deg[i] = 0;
    fill( g[i], g[i] + n , INF);
    //for(int j = 0; j < n; j++)
    //  g[i][j] = INF;
  }

  for(int i = 0; i < m; i++)
  {
    int u,v,cost;
    scanf("%d %d %d", &u, &v, &cost);
    g[u][v] = cost;
    g[v][u] = cost;
    adj[u][deg[u]++] = v;
    adj[v][deg[v]++] = u;
  }

  sol = djikstra(0,n-1);

  printf("%d\n", sol);

  return 0;

}
