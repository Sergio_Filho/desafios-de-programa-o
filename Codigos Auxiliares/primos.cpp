#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <vector>
#define MAX 2147483647
#define MAX_SQRT sqrt(MAX) + 1

using namespace std;

typedef vector <int> vi;

vi eratosthenes(int n)
{
	char* sieve;
	int i, j, m;
  vi primes;

	if(n < 2)
		return primes;

	m = (int) sqrt((double) n);

	/* calloc initializes to zero */
	sieve = (char*)calloc(n+1,sizeof(char));
	sieve[0] = 1;
	sieve[1] = 1;
	for(i = 2; i <= m; i++)
		if(!sieve[i])
			for (j = i*i; j <= n; j += i)
				if(!sieve[j]){
					sieve[j] = 1;
				}

  for(i=2; i<= n; i++)
    if( !sieve[i] )
      primes.push_back(i);

  return primes;
}


int main()
{
  vi primes;
  primes = eratosthenes(MAX_SQRT);
  printf("#define NUM_PRIMES %d\n", primes.size() );
  printf("int primes[NUM_PRIMES] = {\n", primes.size() );
  printf("%5d", primes[0]);
  for(int i = 1; i < primes.size(); i++)
  {
    printf(",");
    if(i%10 == 0) printf("\n");
    printf("%5d", primes[i]);

  }
  printf("};");

}
