void dfs(int v){
	int j;
	marc[v]=1;
	nmarcados++;
	if( nmarcados == n ) return;
	if( pred[t]!=-1) return;
	for(j=0;j < n;j++){
	   if(g[v][j] > 0 && marc[j]==0){
	      pred[j]=v;
		    dfs(j);
	   }
	}
}

int fff(int s, int t){
    int cont=0;
    while (1){
      for (int i=0;i<n;i++){
          pred[i]=-1;
          marc[i]=0;
      }
      nmarcados=0;
      dfs(0);
	    if (pred[t]==-1) break;
      for (int i=t;pred[i]!=-1;i=pred[i]){
          g[pred[i]][i]--;
          g[i][pred[i]]++;
      }
      cont++;
    }
    return cont;
}