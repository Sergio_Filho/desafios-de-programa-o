#include <stdio.h>
#include <iostream>
using namespace std;
int *X_, *Y_, *Z_;

void solve(int X, int Y, int Z, int x, int y, int z){
	if(z == 0) {		
		X_ = &X;
		Y_ = &Y;
		Z_ = &Z;
		return;
	}
	int q = Z/z;
	solve(x, y, z, X-q*x, Y-q*y, Z-q*z);
	
}

int mdc(int a, int b){
	solve(1, 0, a, 0, 1, b);
	return *Z_;
}

int main(){
	int a, b;
	scanf("%d %d", &a, &b);
	mdc(a, b);
	printf("%d %d %d\n", *X_, *Y_, *Z_);
}