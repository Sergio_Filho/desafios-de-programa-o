#include <iostream>
#include <stdio.h>
#include <queue>

using namespace std;

int n, pos, c, t, x, cont;

int main(){
	scanf("%d", &c);

	for(int i=0; i<c; i++){
		priority_queue <int> pq;
		queue<int> fila;
		scanf("%d %d", &n, &pos);
		for (int j=0; j<n; j++){
			scanf("%d", &x);
			pq.push(x);
			fila.push(x);
		}
		t = n;
		cont = 0;
		while (true){
			x = fila.front();
			if(x == pq.top()){				
				cont++;
				fila.pop();
				pq.pop();
				if (pos==0)
					break;
				pos--;
				t--;							
			}else{
				fila.pop();
				fila.push(x);
				if(pos==0)
					pos = t-1;
				else
					pos--;

			}		
			
		}
		printf("%d\n", cont);
	}

	return 0;
}