#include <iostream>
#include <stdio.h>
using namespace std;

int v[31];
int d;
void write(){
	for (int i = 1; i <= d; ++i){
		printf(" %d", v[i]);
	}
}
bool sum(int* s, int tam, int valor){
	bool somas[valor+1];
	somas[0] = true;
	int soma;
	for (int i = 1; i <=valor; ++i){
		somas[i] = false;
	}
	for (int i = 1; i <=tam; ++i){
		for (int j = valor; j >= 0; --j){
			if(somas[j]){
				soma = j + s[i];
				if(soma<=valor)
					somas[soma] = true;
			}
		}
	}
	return somas[valor];
}

int main(){
	int cont = 1;
	v[0] = 0;	
	bool flag;
	while(scanf("%d", &d) != EOF){			
		flag = true;
		for (int i = 1; i <= d; ++i){
			scanf("%d", &v[i]);
			if(v[i]<=v[i-1])
				flag = false;				
			
		}
		if(flag){
			for (int i = 2; i <=d; ++i)	{
				if(sum(v,i-1,v[i])){
					printf("\nCase #%d:",cont);
					write();
					printf("\nThis is not an A-sequence.");
					break;
				}else if(i==d){
					printf("\nCase #%d:",cont);
					write();
					printf("\nThis is an A-sequence.");
				}
			}
		}
		else{
			printf("\nCase #%d:",cont);
			write();
			printf("\nThis is not an A-sequence.");
		}
		cont++;
	}

	
}
