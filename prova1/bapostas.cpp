#include <stdio.h>
#include <iostream>
using namespace std;
int n;
int* v;

int kadane(){
	int max_total = -1;
	int max_atual = 0;
	for (int i=0; i<n; i++){
		max_atual += v[i];
		if(max_atual < 0){
			max_atual = 0;
		}
		if(max_atual>max_total){
			max_total = max_atual;
		}
	}
	return max_total;
}

int main(){
	int total;
	while(true){
		scanf("%d", &n);
		if(n==0) break;
		v = new int[n];
		for (int i = 0; i < n; ++i){
			scanf("%d", &v[i]);
		}
		total = kadane();
		if(total > 0)
			printf("\nThe maximum winning streak is %d.", total);
		else
			printf("\nLosing streak.");
		delete [] v;
	}
	return 0;
}