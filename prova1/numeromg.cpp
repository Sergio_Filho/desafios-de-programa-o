#include <stdio.h>
#include <iostream>
using namespace std;
char a[7], b[7], res[8];

int length(char* c){
	int t = 0;
	while(c[t]) t++;	
	return t;
}

bool equal(char* s1, char* s2){		
	if(length(s1) == length(s2)){				
		for (int i = 0; s1[i]; ++i){
			if(s1[i] != s2[i])
				return false;
		}
		return true;
	}
	else return false;
}

int maior(int a, int b){
	return (a>b ? a: b);		
}

int menor(int a, int b){
	return (a<b ? a: b);		
}

int valorNumerico(char c){
	if(c >= '0' && c <= '9')
		return (c - '0');
	else 
		return (c - 'A' + 10);
}

char valorBase(int i){
	if(i<=9)
		return (char) (i + '0');
	else
		return (char) (i-10 + 'A');	
}

void invert (char* c, int tam){
	char aux;
	for (int i = 0; i < tam/2; ++i)	{
		aux = c[i];
		c[i] = c[tam-1-i];
		c[tam-1-i] = aux;
	}
}



char* soma36 (){
	int rest=0;
	int result, menorN, maiorN, va, vb, i, ta, tb,ind;		
	ta = length(a);
	tb = length(b);	
	ind = 1;

	menorN = menor(ta, tb);
	maiorN = maior(ta, tb);	
	res[0] = '\0';
	int aux2;
	for (i = 0; i<menorN; ++i,++ind){		

		va = valorNumerico(a[ta-1-i]);		
		vb = valorNumerico(b[tb-1-i]);
		
		result = va+vb+rest;

		rest = result>=36 ? result/36 : 0;		
		res[ind] = valorBase(result % 36);			
		
	}

	
	char* aux = ta > tb? a: b;	
	for (int j = maiorN-menorN-1; j >= 0; --j,++ind){
		vb = valorNumerico(aux[j]);		
		result = vb+rest;		
		rest = result>=36 ? result/36 : 0;
		res[ind] = valorBase(result % 36);
	}

	if (rest > 0){
		res[ind] = valorBase(rest);
		ind++;			
	}		
	invert(res, ind);	
}

int main(){	
	char zero[] = {'0','\0'};
	while(true){		
		scanf("%s %s", a, b);		
		if(equal(a, zero) && equal(b, zero))
			break;
		soma36();
			printf("\n%s", res);		
	}
	return 0;
}