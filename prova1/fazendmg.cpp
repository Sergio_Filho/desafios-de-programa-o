#include <stdio.h>
#include <iostream>
#include <string.h>
using namespace std;

int m[1503][1503];
int n, x, y, l, c,area,cerca;

int countCerca(int x, int y){
	int c = 0;	
	if(!m[x][y-1]) c++;
	if(!m[x][y+1]) c++;
	if(!m[x-1][y]) c++;
	if(!m[x+1][y]) c++;	
	return c;
}

int main(){
	while(true){
		area=cerca=0;
		scanf("%d", &n);
		if(n==0) break;
		memset(m, 0, sizeof(m));
		for (int i = 0; i < n; ++i){
			scanf("%d %d %d %d", &x, &y, &l, &c);
			//criar bordas
			x++;
			y++;
			for (int j = x; j < x+l; ++j){
				for (int k = y; k < y+c; ++k){
					m[j][k] = 1;
				}
			}

		}
		for (int i = 0; i < 1503; ++i){
			for (int j = 0; j < 1503; ++j){
				if (m[i][j]){
					area++;
					cerca+=countCerca(i,j);
				}
				
			}	
		}
		printf("\n%d %d",area, cerca );

	}
	return 0;
}
