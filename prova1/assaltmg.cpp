#include <stdio.h>
#include <iostream>
#include <bitset>
using namespace std;

int t, k, d;
long int v[21];

int min(){
	int minimo = 22;
	int elem;
	long int keys;
	for (long int i = 0; i < 1<<d; ++i){		
		keys = 0l;
		for (long int j = 0; j < d; ++j){			
			if(1<<j & i){
				keys = keys | v[j];
			}
		}
		bitset<48> b (keys);
		
		if(b.count()>=k){
			bitset<48> b2 (i);
			elem = b2.count();
			if(elem < minimo){
				minimo = elem;
			}
		}		
	}
	return minimo;
}	

int main(){	
	int r;
	scanf("%d", &t);
	long int aux;
	int c;
	for (int i = 0; i < t; ++i)	{		
		scanf("%d %d", &k, &d);		
		for (int j = 0; j < d; ++j){			
			scanf("%d", &c);			
			v[j] = 0l;
			for (int p = 1; p <= c; ++p){				
				scanf("%ld", &aux);
				v[j] = v[j] | 1<<aux;	
			}			

		}
		r = min();
		if(r<22)
			printf("\n%d", r);
		else
			printf("\nDesastre!");
	}

}