#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <queue>
#include <map>
using namespace std;

typedef pair <int, int> ii;


int l, c;

char labirinto[22][22];
ii vis[22][22][22][22]; //custo minimo para visitar o estado[sx][sy][bx][by];


int x[] = {0,0,1,-1};
int y[] = {1,-1,0,0};

//px + dx[0] , py + dy[0]


class Estado {
  public:
  ii s;
  ii b;
  ii t;
  int movimentos;
  int empurroes;

  Estado()
  {
    movimentos = 0;
    empurroes  = 0;
  }

  Estado(ii s, ii b, ii t)
  {
    this->s = s;
    this->b = b;
    this->t = t;
    movimentos = 0;
    empurroes  = 0;
  }

  bool has_block(int i, int j)
  {
    ii p = make_pair(i,j);
    return p == b;
  }

  bool operator< (const Estado& e) const {
      if( this->s < e.s ) return true;
      if( this->s > e.s ) return false;
      if( this->b < e.b ) return true;
      if( this->b > e.b ) return false;
      return false;
  }



};


void mostra(Estado e)
{
  for(int i = 0; i < l+2; i++)
  {
    for(int j = 0; j < c+2; j++)
    {
      ii p = make_pair(i,j);

      if(p == e.s) printf("S");
      else if( p == e.b) printf("B");
      else if( p == e.t) printf("T");
      else if( labirinto[i][j] == '#') printf("#");
      else printf(".");
    }
    printf("\n");
  }

  printf("movimentos %d\n", e.movimentos);
  printf("empurroes %d\n", e.empurroes);
}


bool has_rock(int i, int j)
{
  return labirinto[i][j] == '#';
}

int distancia(ii p1, ii p2)
{
  return abs(p1.first - p2.first) + abs(p1.second - p2.second);
}

int heuristica(Estado & e)
{
  return distancia(e.b, e.t) + distancia( e.s, e.b  ) - 1;
}



int main()
{

  int instancia = 1;

  while(1)
  {
    scanf("%d %d", &l , &c);

    if( l == 0 && c == 0) break;

    for(int i = 1; i <= l; i++)
      scanf("%s", &labirinto[i][1]);


    for(int j = 0; j < c + 2; j++)
    {
      labirinto[0][j]   = '#';
      labirinto[l+1][j] = '#';
    }

    for(int i = 0; i < l + 2; i++)
    {
      labirinto[i][0]   = '#';
      labirinto[i][c+1] = '#';
    }



    ii s;
    ii b;
    ii t;

    for(int i = 0; i < l+2; i++)
    {
      for(int j = 0; j < c+2; j++)
      {
        if( labirinto[i][j] == 'S')
        {
          s.first = i;
          s.second = j;
        }

        if( labirinto[i][j] == 'T')
        {
          t.first = i;
          t.second = j;
        }

        if( labirinto[i][j] == 'B')
        {
          b.first = i;
          b.second = j;
        }
      }
    }


    Estado inicial(s,b,t);

    int min_empurroes  = -1;
    int min_movimentos = -1;
    long long int nos_visitados = 0LL;

    queue <Estado> fila;

    for(int i = 1; i <= l; i++)
      for(int j = 1; j <= c; j++)
        for(int k = 1; k <= l; k++)
          for(int l = 1; l <= c; l++)
            vis[i][j][k][l] = make_pair(-1,-1);

    fila.push(inicial);

    while( !fila.empty() )
    {
      Estado u = fila.front();

      fila.pop();

      //mostra(u);

      //system("PAUSE");

      if( u.b == u.t )
      {

        if( min_empurroes == -1)
        {
          min_empurroes = u.empurroes;
          min_movimentos = u.movimentos;
        }
        else{

          if( u.empurroes < min_empurroes)
          {
            min_empurroes = u.empurroes;
            min_movimentos = u.movimentos;
          } else if( u.empurroes == min_empurroes && u.movimentos < min_movimentos)
          {
            min_empurroes = u.empurroes;
            min_movimentos = u.movimentos;
          }
        }
      }

      if( min_movimentos != -1)
      {

        if( u.movimentos > min_movimentos)
        {
          continue;
        }

        if( u.empurroes > min_empurroes)
        {
          continue;
        }

        if( min_empurroes - u.empurroes + u.movimentos >= min_movimentos)
        {
          continue;
        }


      }


      int bx,by,sx,sy;
      bx = u.b.first;
      by = u.b.second;
      sx = u.s.first;
      sy = u.s.second;

      ii t = vis[bx][by][sx][sy];
      if( t.first == -1)
      {
        vis[bx][by][sx][sy] = make_pair( u.empurroes, u.movimentos);
      }
      else
      {
        ii t = vis[bx][by][sx][sy];
        if( u.empurroes > t.first ) continue;
        if( u.empurroes == t.first && u.movimentos > t.second ) continue;
        else vis[bx][by][sx][sy] = make_pair(u.empurroes, u.movimentos );
      }


      nos_visitados++;

      for(int k = 0; k < 4; k++)
      {
        int i, j;
        i = u.s.first;
        j = u.s.second;

        if( has_rock(i + x[k],j + y[k])  ) continue;

        if( u.has_block(i + x[k], j + y[k] )  )
        {
          if( has_rock(i + 2*x[k],j + 2*y[k]) ) continue;
          Estado filho;
          filho.s = make_pair(i + x[k], j + y[k]);
          filho.b = make_pair(i + 2*x[k],j + 2*y[k]);
          filho.t = u.t;
          filho.empurroes = u.empurroes + 1;
          filho.movimentos = u.movimentos + 1;
          fila.push(filho);
        }
        else
        {
          Estado filho;
          filho.s = make_pair(i + x[k], j + y[k]);
          filho.b = u.b;
          filho.t = u.t;
          filho.empurroes = u.empurroes ;
          filho.movimentos = u.movimentos + 1;
          fila.push(filho);
        }



      }


    }

    printf("nos visitados %lld\n", nos_visitados );
    printf("Instacia %d\n", instancia++);
    if(min_empurroes != -1)
      printf("%d %d\n", min_movimentos, min_empurroes );
    else
      printf("Impossivel\n");
  }

  return 0;
}
