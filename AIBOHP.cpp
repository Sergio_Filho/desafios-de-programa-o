#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int aibofobia(char* v, int n){
	int **mat = (int**) malloc(n*sizeof(int*));
	for(int i=0;i<n;i++){
		mat[i] = (int*) malloc(n*sizeof(int));
	}
	
	for(int i = 0; i<n-1; i++){
		mat[i][i] = 0;
		mat[i+1][i] = 0;
	}
	mat[0][n-1] = 0;
	
	int aux1, aux2;

	for(int i = n-2; i >= 0; i--){
		for(int j = i+1; j<n; j++){
			if(v[i] == v[j])
				mat[i][j] = mat[i+1][j-1];
			else{
				aux1 = 1 + mat[i+1][j];
				aux2 = 1 + mat[i][j-1];
				mat[i][j] = aux1 < aux2 ? aux1 : aux2;
			}
		}

	}

	int res = mat[0][n-1];
	for(int k=0;k<n;k++){
		free(mat[k]);
	}
	free(mat);
	return res;
}

int main(){

	int t;
	cin >> t;
	string str;
	for(int k=0;k<t;k++){
		cin >> str;
		int tam = str.size();
		char strArray[tam+1];
		strcpy(strArray, str.c_str());
		int res = aibofobia(strArray, tam);
		cout << res << endl;
	}

	return 0;
}
