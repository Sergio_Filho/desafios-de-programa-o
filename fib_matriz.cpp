#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

typedef struct Vetor{
	int *vet;

	Vetor(int * v) : vet(v) {};

	int& operator[](int index){
		return vet[index];
	}
};

typedef struct matriz{
	int n, m;
	int** M;

	matriz(int a, int b) : n(a), m(b){
		M = (int **) malloc(a*sizeof(int *));
		for (int i = 0; i < a; ++i)	
			M[i] = (int*)malloc(b*sizeof(int));			
		
	}

	void mostra(){
		for (int i = 0; i < n; ++i)	{
			for (int j = 0; j < m; ++j)	
				printf("%d ", M[i][j]);
			printf("\n");
		}
	}

	Vetor operator[](int index){
		return Vetor(M[index]);
	}

	matriz& operator *(const matriz& m){
	if (this->m == m.n){
        matriz mat(this->n, m.m);
        for (int i = 0; i < mat.n; ++i){
         	for (int j = 0; j < mat.m; ++j)	{
         		float acc = 0;
            	for (int k=0; k < this->m; k++)
                	acc += (M[i][k] * m.M[k][j]);
                mat[i][j] = acc;
            }
         		
        }
        return mat;
    }                
    else
        throw "multiplicação inválida de matrizes";
	}
	
};

matriz potencia(matriz m, int n){
	if(n == 0){
		matriz mat(2,2);
		mat[0][0] = 1;
		mat[1][1] = 1;
		mat[1][0] = 0;
		mat[0][1] = 0;
		return mat;
	}
	else if(n == 1) return m;
	else if (n%2 == 0) {		
		matriz mat = potencia(m, n/2);		
		return mat*mat;		
	}else{
		matriz mat = potencia(m, n-1);
		return m*mat;
	}


}

int fib(int n){
	if(n == 0 || n == 1) return 1;
	matriz m(2,2);
	m[0][0] = 1;
	m[0][1] = 1;
	m[1][0] = 1;
	m[1][1] = 0;
	matriz mat = potencia(m, n-1);	
	return mat[0][0] + mat[0][1];
}
	

int main(){
	int n;
	scanf("%d", &n);
	printf("%d\n",fib(n));
	
}