#include <iostream>
#include <stdio.h>
#include <math.h>

int esc = 0;
double logaritEsc;
int d, c;
int n;
double logarit;

int main (){
	scanf("%d", &n);
	for (int i = 0; i < n; ++i)	{
		scanf("%d %d", &d,&c);
		logarit = c*log10(d);
		if (logarit>logaritEsc)	{
			esc = i;
			logaritEsc = logarit;
		}	
	}
	printf("%d\n",esc );
	return 0;
}