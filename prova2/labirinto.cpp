#include <queue>
#include <iostream>
#include <stdio.h>
using namespace std;

int labirinto[52][52];
int dx[] = {0, 1, -1, 0, 0};
int dy[] = {0, 0, 0, 1, -1};
int N, M;

struct estado{
	int px, py;
	int turno;	
};

queue<estado> estados;

int bfs(){
	int sx, sy;
	while(!estados.empty()){
		estado e = estados.front();		
		estados.pop();
		if(e.px == N && e.py == M)
			return e.turno;
		for(int i = 0; i<5; i++){
			sx = e.px + dx[i];
			sy = e.py + dy[i];
			if(sx>=1 && sx<=N && sy>=1 && sy<=M){				
				if((labirinto[sx][sy] +e.turno ) %10  <= (labirinto[e.px][e.py]+e.turno+1)%10){
					estado f;
					f.px = sx;
					f.py = sy;
					f.turno = e.turno + 1;
					estados.push(f);
				}
			}
		}		

	}
	return -1;
}

int main(){

	scanf("%d %d", &N, &M);
	for (int i = 1; i <= N; ++i){
		for (int j = 1; j <= M; ++j){
			scanf("%d", &labirinto[i][j]);
		}
	}

	estado inicio;
	inicio.px = 1;
	inicio.py = 1;
	inicio.turno = 0;
	estados.push(inicio);
	printf("%d\n", bfs());
}
