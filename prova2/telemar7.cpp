#include <iostream>
#include <stdio.h>
#include <queue>
#define MAXN 1001
#define MAXV 1000000
using namespace std;
int vendedores[MAXN];
int n, l, t;

void printFila(priority_queue <pair <int, int> > fila){
	int at, temp;
	while (!fila.empty()){		
		at = fila.top().second;
		temp = fila.top().first;
		fila.pop();
		printf("(%d, %d) \n", temp, at);
	}
}

void update(priority_queue <pair <int, int> >& fila, int tempo){
	priority_queue <pair <int, int> > aux;
	int at, temp;	
	while (!fila.empty()){		
		at = fila.top().second;
		temp = fila.top().first;
		fila.pop();
		aux.push(make_pair((temp+tempo), at));
	}
	
	fila = aux;
}


int main(){
	fill(vendedores, vendedores+MAXN, 0);

	priority_queue < pair<int, int> > fila_atendimento;
	queue < int > fila_ligacoes;

	scanf("%d %d", &n, &l);
	for (int i = 0; i < l; ++i){
		scanf("%d", &t);
		fila_ligacoes.push(t);
	}	

	for(int i = 1; i<=n; i++){
		fila_atendimento.push(make_pair(0, -i));
	}
	int at, t, temp;
	while(!fila_ligacoes.empty()){
		at = fila_atendimento.top().second;
		t = fila_atendimento.top().first;
		fila_atendimento.pop();		
		vendedores[-at]++;

		temp = fila_ligacoes.front();		
		fila_ligacoes.pop();

		update(fila_atendimento, -t);
		fila_atendimento.push(make_pair(-temp, at));
	}

	for (int i = 1; i <= n; ++i){
		printf("%d %d\n",i, vendedores[i]);
	}


}
