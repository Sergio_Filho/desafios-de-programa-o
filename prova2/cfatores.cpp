#include <stdio.h>
#include <math.h>
#include <iostream>
#include <bitset>
#include <vector>
#define MAXV 1000000
using namespace std;
vector<int> primos;

void crivo(){
	bitset<1000001> bs;
	bs.set();//sem parametro seta c 1
	bs.set(1, 0);
	bs.set(0, 0);
	for (int i = 2; i <= 1000; ++i)	{//1000 = raiz de 1000000
		for (int j = i*i; j <= MAXV; j=j+i){
			bs.set(j, 0);
		}
	}
	for (int i = 2; i <= MAXV; ++i){
		if(bs.test(i))
			primos.push_back(i);

	}	
}

int main(){
	int n, cont, x;
	crivo();
	while(true){		
		scanf("%d", &n);
		x = n;
		cont=0;
		if(n == 0) break;		
		for (int i = 0; i <= sqrt(n); ++i){
			if(n % primos[i]) continue;
			cont++;
			while( !(n % primos[i]) )
				n = n/primos[i];
			
		}
		if(n != 1) cont++;
		printf("%d : %d\n", x, cont);

	}

	return 0;
}
 