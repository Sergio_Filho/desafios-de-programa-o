#include <iostream>
#include <stdio.h>
#include <vector>
#include <queue>
#define MAXN 1002
#define MAXV 10000000

using namespace std;

int n, m;
int s, t, b;
int d_min[MAXN];
int g[MAXN][MAXN];
bool marcados[MAXN];

int djikstra(int s, int t){
	fill(d_min, d_min+n+2, MAXV);
	fill(marcados, marcados+n+2, false);
	priority_queue < pair<int, int> > fila;

	d_min[0] = 0;
	fila.push(make_pair(0, s));
	int v;
	while(!fila.empty()){
		v = fila.top().second;
		fila.pop();

		if(marcados[v])
			continue;
		marcados[v] = true;
		if(v == t) break;

		for (int u = 0; u <=n+1; ++u){
			if(d_min[v] + g[v][u] < d_min[u]){
				d_min[u] = d_min[v] + g[v][u];
				fila.push(make_pair(-d_min[u], u));
			}
		}

	}
	return d_min[t];
}

int main(){
	scanf("%d %d", &n, &m);
	for (int i = 0; i <= n+1; ++i)
		for (int j = 0; j <= n+1; ++j)
			g[i][j]	= MAXV;
	
	for (int i = 0; i < m; ++i){
		scanf("%d %d %d", &s, &t, &b);				
		g[s][t] = b;
    	g[t][s] = b;
	}

	int r = djikstra(0, n+1);
	printf("%d\n", r);

	return 0;
}