#include <iostream>
#include <stdio.h>
#include <queue>
#define MAXN 101
#define MAXV 10000000

using namespace std;

int n, m, s, d;
int i, j, t;
int d_min[MAXN];
int g[MAXN][MAXN];
bool marcados[MAXN];

int djikstra(int ini, int cheg){
	fill(d_min, d_min+MAXN, MAXV);
	fill(marcados, marcados+MAXN, false);
	priority_queue < pair<int, int> > fila;

	d_min[ini] = 0;		
	fila.push(make_pair(0, ini));
	int v;
	while(!fila.empty()){
		v = fila.top().second;
		fila.pop();

		if(marcados[v])
			continue;
		marcados[v] = true;
		if(v == cheg) break;

		for (int u = 1; u <=n; ++u){
			if(d_min[v] + g[v][u] < d_min[u]){
				d_min[u] = d_min[v] + g[v][u];
				fila.push(make_pair(-d_min[u], u));
			}
		}

	}

	return d_min[cheg] < MAXV? d_min[cheg]:-1;
}

int main(){
	while (true){
		scanf("%d %d", &n, &m);
		if(n==0 && m==0) break;
		for (int k = 0; k <= n; ++k)
			for (int j = 0; j <= n; ++j)
				g[k][j]	= MAXV;
		
		for (int k = 0; k < m; ++k){
			scanf("%d %d %d", &i, &j, &t);				
			g[i][j] = t;
	    	//g[j][i] = t;
		}
		scanf("%d %d", &s, &d);
		int r = djikstra(s, d);
		printf("%d\n", r);
	}

	return 0;
}