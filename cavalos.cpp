#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <queue>
#define MAXN 202
#define INF 0x7FFFFFFF

using namespace std;

int capacity[MAXN][MAXN];
int flow[MAXN][MAXN];
int excess[MAXN];
int height[MAXN];
int nodes;
int src,dest;
queue <int> q;

void addEdge(int u, int v, int c)
{
  capacity[u][v] = c;
}

void push(int u, int v)
{
  int send = min( excess[u], capacity[u][v] - flow[u][v]);

  flow[u][v] += send;
  flow[v][u] -= send;
  excess[u] -= send;
  excess[v] += send;

  if( excess[u] > 0) q.push(u);
  if( excess[v] > 0 && v != dest) q.push(v);


}

void relabel(int u)
{
  int min_height = INF;
  int old_height = height[u];


  for(int v = 0; v < nodes; v++)
  {
    if( capacity[u][v] - flow[u][v] > 0)
    {
      min_height = min( min_height, height[v] );
      height[u] = min_height + 1;
    }
  }

  if( height[u] > old_height)
    q.push(u);

}

int check_node(int i)
{
  if( excess[i] <= 0) return -1;

  //printf("checking\n");
  for(int j = 0; j < nodes; j++)
  {
    //printf("capacidade %d\n", capacity[i][j] - flow[i][j] );
    //printf("height[%d] = %d height[%d] = %d\n",i, height[i], j, height[j]);
    //printf("%d\n", (height[i] == (height[j] + 1) ) );
    if( capacity[i][j] - flow[i][j] > 0 && (height[i] == (height[j] + 1) )  )
    {
      return j;
    }
  }
  return -1;
}

void preflow()
{
  fill( excess, excess + nodes, 0);
  fill( height, height + nodes, 0);

  height[src] = nodes;
  for(int i = 0; i < nodes; i++)
  {
    if( capacity[src][i] > 0)
    {
      flow[src][i] = capacity[src][i];
      flow[i][src] = -capacity[src][i];
      excess[i] = capacity[src][i];
      excess[src] -= capacity[src][i];
      if( i != dest)
      {
        q.push(i);
      }
    }
  }

}

void push_relabel(int _src, int _dest)
{
  src = _src;
  dest = _dest;
  preflow();
  while(!q.empty())
  {
    int current = q.front();
    int target = check_node(current);
    //printf("analisando vertice %d height %d excess %d\n",
    //       current , height[current] , excess[current]);
    if( target >= 0)
    {
      //printf("push\n");
      push(current, target);
    }else{
      if( excess[current] > 0)
      {
        //printf("relabel\n");
        relabel(current);
      }
    }
    q.pop();




  }

  

}



int main(){
    int x,i,u,c,s,n,m,v,cont=1,j;
   
   
    while (scanf ("%i %i %i",&c,&s,&m) > 0){
		memset( capacity, 0, sizeof(capacity));
	  	memset( flow, 0, sizeof(flow));   
		nodes = c+s+2;   
	   	    
	    for (i=1;i<=c;i++){
	       
	        scanf("%i",&capacity[s+i][c+s+1]);       
	        
	    }
	   
	    for (i=1;i<=m;i++){
	        scanf ("%i %i",&u,&v);
	       
	        capacity[v][s+u]=1;
	    }
	   
	    for (i=1;i<=s;i++){
	       
	        capacity[0][i]=1;
	    }
	   
	   
	    push_relabel(0, nodes-1);
	    printf ("Instancia %i\n",cont);
	    printf ("%i",excess[nodes-1]);
	    printf ("\n\n");
	    cont++;
	}
return 0;

}
